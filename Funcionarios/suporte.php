<?php

class Suporte extends Funcionario {

    private $salarioBruto;
    private $salarioLiquido;

    function getSalarioBruto() {
        echo '<br>';
        echo 'Salario Bruto: ';
        echo $this->salarioBruto;
    }

    function getSalarioLiquido() {
        echo '<br>';
        echo 'Salario Liquido: ';
        echo ($this->salarioLiquido - ($this->salarioLiquido * 0.10));
    }

    function setSalarioBruto($salarioBruto) {
        $this->salarioBruto = $salarioBruto;
    }

    function setSalarioLiquido($salarioLiquido) {
        $this->salarioLiquido = $salarioLiquido;
    }

    function darSuporte() {
        echo '<br>';
        echo 'le o erro mula velha, o sistema com mensalidade atrasada nao funciona.';
    }

}

?>