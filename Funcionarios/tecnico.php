<?php

class Tecnico extends Funcionario {

    private $salarioBruto;
    private $salarioLiquido;

    function getSalarioBruto() {
        echo '<br>';
        echo 'Salario Bruto: ';
        echo $this->salarioBruto;
    }

    function getSalarioLiquido() {
        echo '<br>';
        echo 'Salario Liquido: ';
        echo ($this->salarioLiquido - ($this->salarioLiquido * 0.11));
    }

    function setSalarioBruto($salarioBruto) {
        $this->salarioBruto = $salarioBruto;
    }

    function setSalarioLiquido($salarioLiquido) {
        $this->salarioLiquido = $salarioLiquido;
    }

    function reparo() {
        echo '<br>';
        echo 'tem que limpar a memoria';
    }

}

?>