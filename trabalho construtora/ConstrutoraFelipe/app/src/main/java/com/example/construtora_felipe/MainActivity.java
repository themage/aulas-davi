package com.example.construtora_felipe;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    private ImageView operarios;
    private ImageView contatos;
    private ImageView clientes;
    private ImageView materiais;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        materiais = findViewById(R.id.materiais);
        clientes = findViewById(R.id.clientes);
        operarios = findViewById(R.id.operarios);
        contatos = findViewById(R.id.contatos);

        materiais.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, materiais.class));

            }
        });

        contatos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, contatos.class));
            }
        });
        clientes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, clientes.class));
            }
        });

        operarios.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, operarios.class));
            }
        });
    }
}
