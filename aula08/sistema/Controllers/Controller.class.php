<?php

class Controller {

    public function run() {

        $url = isset($_GET['url']) ? $_GET['url'] : "/";
        $url = explode('/', $url);
        

        $controllerAtual = "";
        $metodoAtual = "";
        $parametros = array();

        if ($url[0] != "") {

            $controllerAtual = "Controller" . ucfirst($url[0]);

            array_shift($url);

            if (isset($url[0]) && !empty($url[0])) {
                $metodoAtual = $url[0];
                
                array_shift($url);
            } else {
                $metodoAtual = "index";
            }// fim do if para montar o metodo atual
            
            if(count($url) > 0 ){
                $parametros = $url;
            }

            
        } else {
            $controllerAtual = "controllerHome";
            $metodoAtual = "index";
        }

        $c = new $controllerAtual();
        call_user_func_array(array($c, $metodoAtual),$parametros);
        
        

//        echo "<hr>";
//        echo "Controller Atual:" . $controllerAtual . "<br>";
//        echo "Método Atual:" . $metodoAtual . "<br>";
//        echo "Parametros".print_r($parametros,TRUE);
        
    }

}

?>