<?php
    class Banco{
        private static $DSN = "mysql:dbname=testbanco;host=localhost";
        private static $USER = "root";
        private static $PASS = "";
        private static $instance;

        public static function instanciar(){
            if(!self::$instance){
                try{

                    self::$instance = new PDO(self::$DSN, self::$USER, self::$PASS);

                }catch (Exception $e){
                    $e->getMessage();
                    die();
                }
            }

            return self::$instance;
        }

        public function pesquisar(){
            $data = "SELECT * FROM usuario";
            $data = self::instanciar()->prepare($data);
            $data->execute();
            $data = $data->fetchAll();


        }
    }
?>
